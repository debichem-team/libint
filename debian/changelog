libint (1.2.1-7) UNRELEASED; urgency=medium

  [ Michael Banck ]
  * debian/watch: Removed, no longer useful as all recent upstream releases are
  * from the 2.x branch. 

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Mon, 15 Mar 2021 18:31:29 +0100

libint (1.2.1-6) unstable; urgency=medium

  * debian/libint1.links: File removed.
  * debian/rules (override_dh_auto_install): New rule, runs dh_auto_install and
    then creates the lib*-stable.so.1 symlinks that were formerly handled by
    libint1.links properly in the right multiarch directory (Closes: #985269).

 -- Michael Banck <mbanck@debian.org>  Mon, 15 Mar 2021 18:31:14 +0100

libint (1.2.1-5) unstable; urgency=medium

  * Team upload
  * Add Breaks on psi4 (<< 1:1.3.2+dfsg-1)

 -- Graham Inggs <ginggs@debian.org>  Thu, 04 Mar 2021 14:25:43 +0000

libint (1.2.1-4) unstable; urgency=medium

  * debian/rules (override_dh_auto_configure): Increase angular momentum
    configure options by one.

 -- Michael Banck <mbanck@debian.org>  Sun, 17 Jan 2021 18:50:04 +0100

libint (1.2.1-3) unstable; urgency=medium

  * debian/rules (override_dh_auto_configure): Set cmake's MAX_AM_ERI variable
    to 5 to be more in-line with the autotools build.
  * debian/rules (override_dh_auto_configure): Add ENABLE_XHOST=OFF to cmake
    variables. 
  * debian/rules (override_dh_auto_install): Skip installing the cmake build.
  * debian/libint-dev.install: Install the cmake files directly out of the
    build directory.
  * debian/rules (override_dh_auto_configure): Add --with-pic to configure
    arguments.
  * debian/control (Standards-Version): Bumped to 4.5.1.
  * debian/compat: Bump to 11.
  * debian/rules (override_dh_auto_install,override_dh_install): Targets
    removed.
  * debian/libint1.install,debian/libint-dev.install: Update for multiarch
    libdir.

 -- Michael Banck <mbanck@debian.org>  Sat, 16 Jan 2021 19:50:14 +0100

libint (1.2.1-2) unstable; urgency=medium

  * debian/libint1.links: Reintroduce lib*-stable.so.1 as symlinks (Closes:
    #887864, #888274).

 -- Michael Banck <mbanck@debian.org>  Mon, 29 Jan 2018 13:13:39 +0100

libint (1.2.1-1) unstable; urgency=medium

  * New upstream release.

  [ Michael Banck ]
  * debian/control (Vcs-Browser, Vcs-Svn): Updated. 
  * debian/watch: Updated to Github.
  * debian/copyright: Update to GPL-3+.
  * debian/control (Build-Depends): Added cmake.
  * debian/rules: Build using cmake as well, needed in order to ship .cmake
    files in libint-dev.
  * debian/libint-dev.install: Install cmake files.
  * debian/rules (override_dh_installchangelogs): Update changelog name.
  * debian/control (libint-dbg): Remove package.
  * debian/rules (override_dh_strip): Remove target.

 -- Michael Banck <mbanck@debian.org>  Sat, 06 Jan 2018 15:48:17 +0100

libint (1.1.6-2) unstable; urgency=medium

  * debian/control (Build-Depends): Added dh-autoreconf.
  * debian/rules: Use --with autoreconf instead of --with autotools_dev as dh
    option.
  * debian/rules (AUTOHEADER): New variable; set to true to skip running
    autoheader during dh_autoreconf.

 -- Michael Banck <mbanck@debian.org>  Thu, 06 Oct 2016 09:18:33 +0200

libint (1.1.6-1) unstable; urgency=medium

  * New upstream release.
  * debian/patches/enable_hardening.patch: Removed, applied upstream.
  * debian/rules (override_dh_auto_configure): Decrease libint-max-am to 5 for
    CP2K compatibility. 

 -- Michael Banck <mbanck@debian.org>  Thu, 06 Oct 2016 00:34:31 +0200

libint (1.1.5-2) unstable; urgency=medium

  * debian/rules (override_dh_auto_configure): Increase libint-max-am to 6,
    libderiv-max-am1 to 4 and libderiv-max-am2 to 3.

 -- Michael Banck <mbanck@debian.org>  Thu, 01 Oct 2015 17:52:38 +0200

libint (1.1.5-1) unstable; urgency=low

  [ Daniel Leidert ]
  * New upstream release.
  * debian/control: Dropped DM-Upload-Allowed.
    (Uploaders): Removed myself.
    (Standards-Version): Bumped to 3.9.4.
    (Build-Depends): Added texlive-font-utils for pslatex.
  * debian/copyright: Updated and rewritten in dep5 format.
  * debian/libint1.lintian-overrides: Added.
  * debian/libint-dev.doc-base: Fixed doc-base-uses-applications-section.
  * debian/rules: Enable hardening and parallel mode. Install changelog.
    (override_dh_configure): Set shell to bash.
  * debian/upstream: Added.
  * debian/watch: Fixed.
  * debian/patches/01_fix_doc_makefile.patch: Renamed to
    debian/patches/fix_doc_makefile.patch.
  * debian/patches/enable_hardening.patch: Added.
    - Fix build with hardening flags enabled.
  * debian/patches/fix_makefiles.patch: Added.
    - Enable LDFLAGS for libraries.
  * debian/patches/series: Adjusted.

 -- Debichem Team <debichem-devel@lists.alioth.debian.org>  Sun, 26 May 2013 22:44:51 +0200

libint (1.1.4-1) unstable; urgency=low

  * Initial upload (Closes: #425039).

 -- Michael Banck <mbanck@debian.org>  Sun, 12 Feb 2012 22:15:45 +0100
